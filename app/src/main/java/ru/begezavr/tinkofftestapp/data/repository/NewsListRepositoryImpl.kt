package ru.begezavr.tinkofftestapp.data.repository

import io.reactivex.Single
import ru.begezavr.tinkofftestapp.data.cache.Cache
import ru.begezavr.tinkofftestapp.data.model.NewsListItemEntity
import ru.begezavr.tinkofftestapp.data.model.NewsListItemMapper
import ru.begezavr.tinkofftestapp.data.remote.NewsListApi
import ru.begezavr.tinkofftestapp.domain.model.NewsListItem
import ru.begezavr.tinkofftestapp.domain.repository.NewsListRepository
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class NewsListRepositoryImpl @Inject constructor(
        private val api: NewsListApi,
        private val cache: Cache<List<NewsListItemEntity>>,
        private val mapper: NewsListItemMapper
) : NewsListRepository {
    override val key = "News List"

    override fun get(refresh: Boolean): Single<List<NewsListItem>> =
            when (refresh) {
                true -> api.getNewsList()
                        .flatMap { set(it.payload) }
                        .map { mapper.mapToDomain(it) }
                false -> cache.load(key)
                        .map { mapper.mapToDomain(it) }
                        .onErrorResumeNext { get(true) }
            }

    private fun set(list: List<NewsListItemEntity>) = cache.save(key, list)
}