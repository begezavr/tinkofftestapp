package ru.begezavr.tinkofftestapp.data.repository

import ru.begezavr.tinkofftestapp.data.cache.Cache
import ru.begezavr.tinkofftestapp.domain.repository.NewsContentRepository
import io.reactivex.Single
import ru.begezavr.tinkofftestapp.data.model.NewsContentEntity
import ru.begezavr.tinkofftestapp.data.model.NewsContentMapper
import ru.begezavr.tinkofftestapp.data.remote.NewsContentApi
import ru.begezavr.tinkofftestapp.domain.model.NewsContent
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class NewsContentRepositoryImpl @Inject constructor(
        private val api: NewsContentApi,
        private val cache: Cache<NewsContentEntity>,
        private val mapper: NewsContentMapper
) : NewsContentRepository {
    override val key = "News Content"

    override fun get(id: String, refresh: Boolean): Single<NewsContent> =
            when (refresh) {
                true -> api.getNewsContent(id)
                        .flatMap { set(id, it.payload) }
                        .map { mapper.mapToDomain(it) }
                false -> cache.load(key + id)
                        .map { mapper.mapToDomain(it) }
                        .onErrorResumeNext { get(id, true) }
            }

    private fun set(postId: String, newsContent: NewsContentEntity) = cache.save(key + postId, newsContent)
}