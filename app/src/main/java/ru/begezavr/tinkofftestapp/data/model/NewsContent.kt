package ru.begezavr.tinkofftestapp.data.model

import com.squareup.moshi.Json
import ru.begezavr.tinkofftestapp.domain.model.NewsContent
import javax.inject.Inject

data class NewsContentPayloadEntity(
        @Json(name = "payload") val payload: NewsContentEntity
)

data class NewsContentEntity(
        @Json(name = "text") val title: NewsContentTitleEntity,
        @Json(name = "creationDate") val creationDate: NewsContentCreationDateEntity,
        @Json(name = "content") val content: String
)

data class NewsContentTitleEntity(
        @Json(name = "id") val id: String,
        @Json(name = "text") val text: String
)

data class NewsContentCreationDateEntity(
        @Json(name = "milliseconds") val milliseconds: Long
)

class NewsContentMapper @Inject constructor() {
    fun mapToDomain(entity: NewsContentPayloadEntity): NewsContent = mapToDomain(entity.payload)

    fun mapToDomain(entity: NewsContentEntity): NewsContent {
        val (id, title) = mapToDomain(entity.title)
        return NewsContent(
                id,
                title,
                entity.content,
                mapToDomain(entity.creationDate)
        )
    }

    fun mapToDomain(entity: NewsContentTitleEntity): Pair<String, String> = Pair(entity.id, entity.text)

    fun mapToDomain(entity: NewsContentCreationDateEntity): Long = entity.milliseconds
}