package ru.begezavr.tinkofftestapp.data.model

import com.squareup.moshi.Json
import ru.begezavr.tinkofftestapp.domain.model.NewsListItem
import javax.inject.Inject


data class NewsListItemsPayloadEntity(
        @Json(name = "payload") val payload: List<NewsListItemEntity>
)

data class NewsListItemEntity(
        @Json(name = "id") val id: String,
        @Json(name = "text") val text: String,
        @Json(name = "publicationDate") val publicationDate: PublicationDateEntity
)

data class PublicationDateEntity (
        @Json(name = "milliseconds") val milliseconds: Long
)

class NewsListItemMapper @Inject constructor() {

    fun mapToDomain(entity: NewsListItemsPayloadEntity): List<NewsListItem> = mapToDomain(entity.payload)

    fun mapToDomain(entity: NewsListItemEntity): NewsListItem = NewsListItem(
            entity.id,
            entity.text,
            mapToDomain(entity.publicationDate)
    )

    fun mapToDomain(entity: PublicationDateEntity): Long = entity.milliseconds

    fun mapToDomain(list: List<NewsListItemEntity>): List<NewsListItem> = list.map { mapToDomain(it) }
}