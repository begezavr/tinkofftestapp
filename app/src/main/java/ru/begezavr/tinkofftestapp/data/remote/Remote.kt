package ru.begezavr.tinkofftestapp.data.remote


import ru.begezavr.tinkofftestapp.data.model.NewsListItemEntity
import ru.begezavr.tinkofftestapp.data.model.NewsContentEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.begezavr.tinkofftestapp.data.model.NewsContentPayloadEntity
import ru.begezavr.tinkofftestapp.data.model.NewsListItemsPayloadEntity

interface NewsListApi {

    @GET("news/")
    fun getNewsList(): Single<NewsListItemsPayloadEntity>
}

interface NewsContentApi {

    @GET("news_content/")
    fun getNewsContent(@Query("id") postId: String): Single<NewsContentPayloadEntity>
}