package ru.begezavr.tinkofftestapp.domain.usecase

import io.reactivex.Single
import ru.begezavr.tinkofftestapp.domain.model.NewsListItem
import ru.begezavr.tinkofftestapp.domain.repository.NewsListRepository
import javax.inject.Inject


class NewsListUseCase @Inject constructor (private val repository: NewsListRepository) {
    fun get(refresh: Boolean): Single<List<NewsListItem>> =
            repository.get(refresh)
}