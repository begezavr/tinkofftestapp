package ru.begezavr.tinkofftestapp.domain.repository

import io.reactivex.Single
import ru.begezavr.tinkofftestapp.domain.model.NewsContent


interface NewsContentRepository {
    val key: String
    fun get(id: String, refresh: Boolean): Single<NewsContent>
}