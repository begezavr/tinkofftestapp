package ru.begezavr.tinkofftestapp.domain.model


data class NewsListItem(
    val id: String,
    val title: String,
    val date: Long
)