package ru.begezavr.tinkofftestapp.domain.repository


import io.reactivex.Single
import ru.begezavr.tinkofftestapp.domain.model.NewsListItem


interface NewsListRepository {
    val key: String
    fun get(refresh: Boolean): Single<List<NewsListItem>>
}