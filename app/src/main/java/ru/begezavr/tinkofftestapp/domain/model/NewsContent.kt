package ru.begezavr.tinkofftestapp.domain.model


data class NewsContent(
    val id: String,
    val title: String,
    val content: String,
    val date: Long
)