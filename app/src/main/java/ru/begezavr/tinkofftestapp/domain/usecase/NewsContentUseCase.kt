package ru.begezavr.tinkofftestapp.domain.usecase

import io.reactivex.Single
import ru.begezavr.tinkofftestapp.domain.model.NewsContent
import ru.begezavr.tinkofftestapp.domain.repository.NewsContentRepository
import javax.inject.Inject


class NewsContentUseCase @Inject constructor (private val repository: NewsContentRepository) {
    fun get(id: String, refresh: Boolean): Single<NewsContent> =
            repository.get(id, refresh)
}