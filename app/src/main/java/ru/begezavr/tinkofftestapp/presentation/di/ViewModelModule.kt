package ru.begezavr.tinkofftestapp.presentation.di

import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import ru.begezavr.tinkofftestapp.presentation.news.NewsViewModelFactory


@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: NewsViewModelFactory): ViewModelProvider.Factory
}