package ru.begezavr.tinkofftestapp.presentation.model

import java.util.*
import ru.begezavr.tinkofftestapp.domain.model.NewsListItem as NewsListItemDomain
import javax.inject.Inject


data class NewsListItem(
        val id: String,
        val title: String,
        val date: Long
)

class NewsListItemMapper @Inject constructor() {

    fun mapToPresentation(newsList: List<NewsListItemDomain>): List<NewsListItem> =
            newsList.map { NewsListItem(it.id, it.title, it.date) }
}