package ru.begezavr.tinkofftestapp.presentation

import android.app.Activity
import android.arch.lifecycle.*
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import ru.begezavr.tinkofftestapp.presentation.di.Injector


val Activity.app: TinkoffNewsApp get() = application as TinkoffNewsApp

fun AppCompatActivity.getAppInjector(): Injector = (app).injector
