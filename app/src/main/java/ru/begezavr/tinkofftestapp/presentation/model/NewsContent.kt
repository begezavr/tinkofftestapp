package ru.begezavr.tinkofftestapp.presentation.model

import ru.begezavr.tinkofftestapp.domain.model.NewsContent as NewsContentDomain
import javax.inject.Inject


data class NewsContent(
        val id: String,
        val title: String,
        val date: Long,
        val content: String
)

class NewsContentMapper @Inject constructor() {

    fun mapToPresentation(it: NewsContentDomain): NewsContent =
            NewsContent(it.id, it.title, it.date, it.content)
}