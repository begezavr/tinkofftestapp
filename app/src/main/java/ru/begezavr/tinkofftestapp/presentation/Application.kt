package ru.begezavr.tinkofftestapp.presentation

import android.app.Application
import com.pacoworks.rxpaper2.RxPaperBook
import io.reactivex.plugins.RxJavaPlugins
import ru.begezavr.tinkofftestapp.presentation.di.AppModule
import ru.begezavr.tinkofftestapp.presentation.di.DaggerInjector
import ru.begezavr.tinkofftestapp.presentation.di.Injector


class TinkoffNewsApp : Application() {

    lateinit var injector: Injector private set

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initRxPaper()
    }

    private fun initDagger() {
        injector = DaggerInjector
                .builder()
                .appModule(AppModule(this))
                .build()
    }


    private fun initRxPaper() = RxPaperBook.init(this)
}