package ru.begezavr.tinkofftestapp.presentation.news

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import ru.begezavr.tinkofftestapp.domain.usecase.NewsContentUseCase
import ru.begezavr.tinkofftestapp.domain.usecase.NewsListUseCase
import ru.begezavr.tinkofftestapp.presentation.model.NewsContentMapper
import ru.begezavr.tinkofftestapp.presentation.model.NewsListItemMapper
import javax.inject.Inject


class NewsViewModelFactory @Inject constructor(
        private val newsContentUseCase: NewsContentUseCase,
        private val newsListUseCase: NewsListUseCase,
        private val newsContentMapper: NewsContentMapper,
        private val newsListItemMapper: NewsListItemMapper
): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(NewsViewModel::class.java)) {
            NewsViewModel(this.newsContentUseCase, this.newsListUseCase, this.newsContentMapper, this.newsListItemMapper) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}