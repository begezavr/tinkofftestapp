package ru.begezavr.tinkofftestapp.presentation.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.begezavr.tinkofftestapp.data.remote.NewsContentApi
import ru.begezavr.tinkofftestapp.data.remote.NewsListApi
import javax.inject.Singleton


@Module
class NetworkModule {

    companion object {
        private const val BASE_URL = "https://api.tinkoff.ru/v1/"
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    fun provideNewsListApi(retrofit: Retrofit): NewsListApi = retrofit.create(NewsListApi::class.java)

    @Provides
    fun provideNewsContentApi(retrofit: Retrofit): NewsContentApi = retrofit.create(NewsContentApi::class.java)
}