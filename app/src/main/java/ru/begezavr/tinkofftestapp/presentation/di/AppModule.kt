package ru.begezavr.tinkofftestapp.presentation.di


import dagger.Module
import dagger.Provides
import ru.begezavr.tinkofftestapp.presentation.TinkoffNewsApp
import javax.inject.Singleton

@Module
class AppModule(val app: TinkoffNewsApp) {

    @Provides
    @Singleton
    fun provideApp(): TinkoffNewsApp = app
}