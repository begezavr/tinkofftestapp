package ru.begezavr.tinkofftestapp.presentation.di

import dagger.Component
import ru.begezavr.tinkofftestapp.presentation.MainActivity
import javax.inject.Singleton


@Singleton
@Component(modules = [ViewModelModule::class, AppModule::class, NetworkModule::class, RepositoryModule::class])
interface Injector {
    fun inject(activity: MainActivity)
}