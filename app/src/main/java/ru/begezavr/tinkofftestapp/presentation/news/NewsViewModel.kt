package ru.begezavr.tinkofftestapp.presentation.news

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import ru.begezavr.tinkofftestapp.domain.usecase.NewsContentUseCase
import ru.begezavr.tinkofftestapp.domain.usecase.NewsListUseCase
import ru.begezavr.tinkofftestapp.presentation.model.NewsContent
import ru.begezavr.tinkofftestapp.presentation.model.NewsContentMapper
import ru.begezavr.tinkofftestapp.presentation.model.NewsListItem
import ru.begezavr.tinkofftestapp.presentation.model.NewsListItemMapper
import javax.inject.Inject


/**
 * Seems like app is simple enough for only one model,
 * but it is easy to divide it to NewsNavigationViewModel, NewsListViewModel
 * and NewsContentViewModel, for example
 */

enum class LoadEvent {
    LOADING, COMPLETE, ERROR
}

class NewsViewModel @Inject constructor(
        private val newsContentUseCase: NewsContentUseCase,
        private val newsListUseCase: NewsListUseCase,
        private val newsContentMapper: NewsContentMapper,
        private val newsListMapper: NewsListItemMapper
) : ViewModel() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    val newsList = MutableLiveData<List<NewsListItem>>()
    val newsContent = MutableLiveData<NewsContent>()
    val loadEvents = MutableLiveData<LoadEvent>()

    val selectedItem = PublishSubject.create<NewsListItem>() as PublishSubject<NewsListItem>

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun loadNewsList(noCache: Boolean = false) = disposables.add(newsListUseCase.get(noCache)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { loadEvents.postValue(LoadEvent.LOADING)}
            .doOnSuccess { loadEvents.postValue(LoadEvent.COMPLETE)}
            .doOnError { loadEvents.postValue(LoadEvent.ERROR)}
            .map { newsListMapper.mapToPresentation(it) }
            .subscribe({ list ->
                val sorted = list.sortedWith(compareByDescending(NewsListItem::date))
                newsList.postValue(sorted)
            }, { })
    )

    fun getNewsContent(id: String) = disposables.add(newsContentUseCase.get(id, false)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { loadEvents.postValue(LoadEvent.LOADING)}
            .doOnSuccess { loadEvents.postValue(LoadEvent.COMPLETE)}
            .doOnError { loadEvents.postValue(LoadEvent.ERROR)}
            .map { newsContentMapper.mapToPresentation(it) }
            .subscribe({ it ->
                newsContent.postValue(it)
            }, { })
    )

    fun selectItem(itemSelections: Observable<NewsListItem>) {
        disposables.add(itemSelections.subscribe { selectedItem.onNext(it) })
    }
}