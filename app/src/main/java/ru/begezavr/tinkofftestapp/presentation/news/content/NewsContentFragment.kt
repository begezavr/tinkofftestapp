package ru.begezavr.tinkofftestapp.presentation.news.content

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.begezavr.tinkofftestapp.R
import ru.begezavr.tinkofftestapp.presentation.format
import ru.begezavr.tinkofftestapp.presentation.model.NewsContent
import ru.begezavr.tinkofftestapp.presentation.news.LoadEvent
import ru.begezavr.tinkofftestapp.presentation.news.NewsViewModel


class NewsContentFragment: Fragment() {
    companion object {
        const val TAG: String = "NewsContentFragment"
        const val NEWS_CONTENT_ID_BUNDLE_KEY: String = "news_content_id_bundle_key"

        @JvmStatic
        fun newInstance(id: String) = NewsContentFragment().apply {
            arguments = Bundle().apply {
                putString(NEWS_CONTENT_ID_BUNDLE_KEY, id)
            }
        }
    }

    private lateinit var model: NewsViewModel
    private lateinit var title: TextView
    private lateinit var content: TextView
    private lateinit var date: TextView

    private lateinit var id: String

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        arguments?.getString(NEWS_CONTENT_ID_BUNDLE_KEY)?.let {
             id = it
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.news_content, container, false) as View
        title = view.findViewById(R.id.news_content_title)
        content = view.findViewById(R.id.news_content_content)
        date = view.findViewById(R.id.news_content_date)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model = ViewModelProviders.of(activity!!).get(NewsViewModel::class.java)
        model.newsContent.observe(this, newsContentObserver)
        model.loadEvents.observe(this, loadEventsObserver)
        model.getNewsContent(id)
    }

    @Suppress("DEPRECATION")
    private val newsContentObserver = Observer<NewsContent> { newData ->
        if (newData != null) {
            title.text = newData.title
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                content.text = Html.fromHtml(newData.content, Html.FROM_HTML_MODE_LEGACY)
            } else {
                content.text = Html.fromHtml(newData.content)
            }
            date.text = format(newData.date)
        }
    }

    private val loadEventsObserver = Observer<LoadEvent> { event ->
        when (event) {
            LoadEvent.LOADING -> {
                title.text = "..."
                content.text = "..."
                date.text = ""
            }
            LoadEvent.COMPLETE -> {/*nothing*/}
            else -> {
                // todo show error
            }
        }
    }
}
