package ru.begezavr.tinkofftestapp.presentation

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import ru.begezavr.tinkofftestapp.R
import ru.begezavr.tinkofftestapp.presentation.model.NewsListItem
import ru.begezavr.tinkofftestapp.presentation.news.NewsViewModel
import ru.begezavr.tinkofftestapp.presentation.news.NewsNavigationManager
import javax.inject.Inject


class MainActivity: AppCompatActivity()  {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var navigationManager: NewsNavigationManager

    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getAppInjector().inject(this)

        navigationManager = NewsNavigationManager(supportFragmentManager, R.id.fragments_placeholder)

        val model = ViewModelProviders.of(this, viewModelFactory).get(NewsViewModel::class.java)
        disposables.add(model.selectedItem.subscribe { it ->
            if (it != null) {
                navigationManager.openNewsContent(it.id)
            }
        })

        if (savedInstanceState == null) {
            navigationManager.openNewsList()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}
