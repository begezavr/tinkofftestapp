package ru.begezavr.tinkofftestapp.presentation.news.list

import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import ru.begezavr.tinkofftestapp.R
import ru.begezavr.tinkofftestapp.presentation.format
import ru.begezavr.tinkofftestapp.presentation.model.NewsListItem
import java.util.*


class NewsListAdapter(private val dataset: ArrayList<NewsListItem>) :
        RecyclerView.Adapter<NewsListAdapter.ViewHolder>() {

    class ViewHolder(newsItemView: View) : RecyclerView.ViewHolder(newsItemView) {
        val name = newsItemView.findViewById(R.id.news_list_item_name) as TextView
        val date = newsItemView.findViewById(R.id.news_list_item_date) as TextView
    }

    private val itemClicks: PublishSubject<NewsListItem> = PublishSubject.create()

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): NewsListAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.news_list_item, parent, false)
        val vh = ViewHolder(view)
        view.setOnClickListener { itemClicks.onNext(dataset[vh.adapterPosition]) }

        return vh
    }

    @Suppress("DEPRECATION")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.name.text = Html.fromHtml(dataset[position].title, Html.FROM_HTML_MODE_LEGACY)
        } else {
            holder.name.text = Html.fromHtml(dataset[position].title)
        }
        holder.date.text = format(dataset[position].date)
    }

    override fun getItemCount() = dataset.size

    override fun getItemId(position: Int): Long {
        return dataset[position].id.hashCode().toLong()
    }

    fun itemClicks(): Observable<NewsListItem> {
        return itemClicks
    }

    fun setData(data: List<NewsListItem>) {
        dataset.clear()
        dataset.addAll(data)
        notifyDataSetChanged()
    }
}