package ru.begezavr.tinkofftestapp.presentation

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

const val DATE_FORMAT = "dd.MM.yyyy"

@SuppressLint("SimpleDateFormat")
val dateFormat = SimpleDateFormat(DATE_FORMAT)

fun format(date: Long) = dateFormat.format(Date(date)) ?: ""