package ru.begezavr.tinkofftestapp.presentation.news

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import ru.begezavr.tinkofftestapp.presentation.news.content.NewsContentFragment
import ru.begezavr.tinkofftestapp.presentation.news.list.NewsListFragment


class NewsNavigationManager(private val fragmentManager: FragmentManager, private val containerId: Int) {

    fun openNewsList() {
        val fragment = NewsListFragment()
        add(fragment, NewsListFragment.TAG, false)
    }

    fun openNewsContent(id: String) {
        val fragment = NewsContentFragment.newInstance(id)
        add(fragment, NewsContentFragment.TAG, true)
    }

    private fun add(fragment: Fragment, tag: String, backstack: Boolean) {
        val ft = fragmentManager.beginTransaction()
        ft.replace(containerId, fragment, tag)
        if (backstack) {
            ft.addToBackStack(tag)
        }
        ft.commit()
    }
}