package ru.begezavr.tinkofftestapp.presentation.di

import dagger.Binds
import dagger.Module
import ru.begezavr.tinkofftestapp.data.repository.NewsContentRepositoryImpl
import ru.begezavr.tinkofftestapp.data.repository.NewsListRepositoryImpl
import ru.begezavr.tinkofftestapp.domain.repository.NewsContentRepository
import ru.begezavr.tinkofftestapp.domain.repository.NewsListRepository


@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindNewsListRepository(repository: NewsListRepositoryImpl): NewsListRepository

    @Binds
    abstract fun bindNewsContentRepository(repository: NewsContentRepositoryImpl): NewsContentRepository
}