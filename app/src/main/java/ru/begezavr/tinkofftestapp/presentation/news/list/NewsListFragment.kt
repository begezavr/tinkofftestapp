package ru.begezavr.tinkofftestapp.presentation.news.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.begezavr.tinkofftestapp.R
import ru.begezavr.tinkofftestapp.presentation.model.NewsListItem
import ru.begezavr.tinkofftestapp.presentation.news.LoadEvent
import ru.begezavr.tinkofftestapp.presentation.news.NewsViewModel


class NewsListFragment: Fragment() {
    companion object {
        const val TAG: String = "NewsListFragment"
    }

    private lateinit var newsView: RecyclerView
    private lateinit var newsAdapter: NewsListAdapter
    private lateinit var newsLayoutManager: RecyclerView.LayoutManager

    private lateinit var swipeContainer: SwipeRefreshLayout

    private lateinit var model: NewsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.news_list, container, false)
        swipeContainer = view.findViewById(R.id.news_list_swipe_to_refresh_container)
        swipeContainer.setOnRefreshListener { model.loadNewsList(true) }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        newsLayoutManager = LinearLayoutManager(activity)
        newsAdapter = NewsListAdapter(arrayListOf())
        newsView = activity!!.findViewById<RecyclerView>(R.id.news_list_recycler_view).apply {
            layoutManager = newsLayoutManager
            adapter = newsAdapter
        }

        model = ViewModelProviders.of(activity!!).get(NewsViewModel::class.java)
        model.newsList.observe(this, newsListObserver)
        model.loadEvents.observe(this, loadEventsObserver)
        model.loadNewsList()
        model.selectItem(newsAdapter.itemClicks())
    }

    private val newsListObserver = Observer<List<NewsListItem>> { newData ->
        if (newData != null) {
            newsAdapter.setData(newData)
            newsAdapter.notifyDataSetChanged()
        }
    }

    private val loadEventsObserver = Observer<LoadEvent> { event ->
        when (event) {
            LoadEvent.LOADING -> {}
            LoadEvent.COMPLETE -> swipeContainer.isRefreshing = false
            else -> {
                // todo show error
            }
        }
    }
}
